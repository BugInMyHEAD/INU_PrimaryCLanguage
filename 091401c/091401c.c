#include <stdio.h>

int main(void)
{
	int x, y;
	printf("첫 번째 숫자를 입력하시오:");
	scanf_s("%d", &x);
	printf("두 번째 숫자를 입력하시오:");
	scanf_s("%d", &y);

	printf("두 수의 합=%d\n", x + y);
	printf("두 수의 차=%d\n", x - y);
	printf("두 수의 곱=%d\n", x * y);
	printf("두 수의 나눗셈몫=%d\n", x / y);

	return 0;
}