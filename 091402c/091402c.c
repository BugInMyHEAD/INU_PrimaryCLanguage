// <구구단>

#include <stdio.h>

int main(void)
{
	int maxMultiple = 3;
	
	int numberFromUser;
	scanf_s("%d", &numberFromUser);
	for (int i2 = 1; i2 <= maxMultiple; i2 += 1)
	{
		printf("%d * %d = %d\n", numberFromUser, i2, numberFromUser * i2);
	}
}

// </구구단>