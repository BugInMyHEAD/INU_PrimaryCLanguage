// <구구단_double>

#include <stdio.h>
#include <float.h>

#define EPSILON (0x1000 * DBL_EPSILON)

int main(void)
{
	double maxMultiple = 3;
	
	double numberFromUser;
	scanf_s("%lf", &numberFromUser);
	for (int i2 = 1; i2 <= maxMultiple + EPSILON; i2 += 1)
	{
		printf("%f * %f = %f\n", numberFromUser, (double)i2, numberFromUser * i2);
	}
}

// </구구단_double>