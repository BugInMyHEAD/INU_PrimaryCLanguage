// <환전_10000원>

#include <stdio.h>

int main(void)
{
	double dollar2Won = 1132.50;
	double euro2Won = 1346.03;
	double yen2Won = 10.2401;
	double yuan2Won = 172.97;

	double wonInMyWallet = 10000.0;
	
	printf("%.0f원 =\n", wonInMyWallet);
	printf("%f dollars\n", wonInMyWallet / dollar2Won);
	printf("%f euro\n", wonInMyWallet / euro2Won);
	printf("%f yen\n", wonInMyWallet / yen2Won);
	printf("%f yuan\n", wonInMyWallet / yuan2Won);
}

// </환전_10000원>