#include <stdio.h>
#include <stdlib.h>


#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )


int main(void)
{
	size_t typeSizeArr[] = { sizeof(char), sizeof(short), sizeof(int), sizeof(long), sizeof(long long), sizeof(float), sizeof(double), sizeof(long double) };
	char* typeStrArr[] = { "char", "short", "int", "long", "long long", "float", "double", "long double" };
	for (int i2 = 0; i2 < ARRLEN(typeSizeArr); i2++)
	{
		printf("%s: %zd 바이트 %zd 비트\n", typeStrArr[i2], typeSizeArr[i2], BYTE2BITS(typeSizeArr[i2]));
		printf("%d %#x %#o\n", 0x7f, 0x7f, 0x7f);
	}

	return 0;
}