#include <stdio.h>


#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )


enum { USD, JPY, EUR, CNY, KRW };
const double EXCHANGERATE[] =
{
	1.0, 1.1238, 1.0 / 1.1888, 6.5906, 1132.60
};

const char EUN[] = "은";
const char NEUN[] = "는";


int main(void)
{
	char* currencyKorName[] = { "달러", "엔", "유로", "위안", "원" };

	char* eunNeun[] = { NEUN, EUN, NEUN, EUN, EUN };

	for (int i2 = 0; i2 < ARRLEN(currencyKorName) - 1; i2++)
	{
		printf("환전할 %s화를 입력하시오: ", currencyKorName[i2]);
		double doubleFromUser;
		scanf_s("%lf", &doubleFromUser);
		printf("%.2f%s%s %.2f%s입니다.\n",
			doubleFromUser, currencyKorName[i2], eunNeun[i2],
			doubleFromUser / EXCHANGERATE[i2] * EXCHANGERATE[KRW], currencyKorName[KRW]);
	}

	return 0;
}