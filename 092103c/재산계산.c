#include <stdio.h>


#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )


enum { USD, JPY, EUR, CNY, KRW };
const double EXCHANGERATE[] =
{
	1.0, 1.1238, 1.0 / 1.1888, 6.5906, 1132.60
};

const char EUN[] = "은";
const char NEUN[] = "는";


int main(void)
{
	printf("매달 저축 금액을 입력하시오: ");
	int amntFromUser;
	scanf_s("%d", &amntFromUser);

	printf("몇 년 후?: ");
	int yearFromUser;
	scanf_s("%d", &yearFromUser);

	printf("%d년 후의 재산 = %lld원\n",  yearFromUser, (long long)12 * amntFromUser * yearFromUser);

	return 0;
}