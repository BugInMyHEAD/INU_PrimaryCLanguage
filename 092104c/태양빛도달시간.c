#include <stdio.h>


#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )


const double LIGHT_SPEED = 300000.0;


int main(void)
{
	double distance = 149600000.0;
	printf("빛의 속도는 %f\n", LIGHT_SPEED);
	printf("태양과 지구와의 거리 %.0f\n", distance);
	double laptimeInSec = distance / LIGHT_SPEED;
	printf("도달 시간은 %f초(%f분)\n", laptimeInSec, laptimeInSec / 60);

	return 0;
}