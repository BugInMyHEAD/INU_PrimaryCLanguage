#include <stdio.h>


#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )


double toFarenheit(double celsius)
{
	return celsius * 1.8 + 32;
}

double toCelsius(double farenheit)
{
	return ( farenheit - 32 ) / 1.8;
}

int main(void)
{
	char farenheitStr[] = "ȭ���µ�";
	char celsiusStr[] = "�����µ�";

	double doubleFromUser;

	printf("%s=", farenheitStr);
	scanf_s("%lf", &doubleFromUser);
	printf("%s=%f\n", celsiusStr, toCelsius(doubleFromUser));

	printf("%s=", celsiusStr);
	scanf_s("%lf", &doubleFromUser);
	printf("%s=%f\n", farenheitStr, toFarenheit(doubleFromUser));

	return 0;
}