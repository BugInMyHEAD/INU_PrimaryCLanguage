// <원의_면적_둘레>

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>

int main(void)
{
	printf("원의 반지름을 입력하시오:");
	double radiusFromUser;
	scanf_s("%lf", &radiusFromUser);
	printf("원의 면적: %.10f\n", M_PI * radiusFromUser * radiusFromUser);
	printf("원의 둘레: %.10f\n", 2 * M_PI * radiusFromUser);

	return 0;
}

// </원의_면적_둘레>