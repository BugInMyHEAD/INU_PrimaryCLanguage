// <문자_숫자>

#include <stdio.h>

int main(void)
{
	printf("문자를 입력하시오:");
	char charFromUser;
	scanf_s("%c", &charFromUser);
	printf("입력된 문자는 %4c입니다\n", charFromUser);
	printf("입력된 문자는 %4d입니다\n", charFromUser);
	printf("입력된 문자는 %#4x입니다\n", charFromUser);

	return 0;
}

// </문자_숫자>