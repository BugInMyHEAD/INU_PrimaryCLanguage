// <자동판매기>

#include <stdio.h>

int main(void)
{
	printf("투입한 돈: ");
	int inputMoneyFromUser;
	scanf_s("%d", &inputMoneyFromUser);

	printf("물건값: ");
	int totalFromUser;
	scanf_s("%d", &totalFromUser);

	int remainder = inputMoneyFromUser - totalFromUser;

	printf("거스름돈: %d\n\n", remainder);
	printf("100원 동전의 개수: %d\n", remainder / 100);
	printf("10원 동전의 개수: %d\n", remainder % 100 / 10);

	return 0;
}

// </자동판매기>