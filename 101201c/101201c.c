// <First>

#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	char guideStr[] = " 번째 정수를 입력하시오: ";
	char * orderStr[] = { "첫", "두" };
	int intFromUser[ARRLEN(orderStr)];

	// Receiving integers from user
	for (int i2 = 0; i2 < ARRLEN(orderStr); i2++)
	{
		printf("%s%s", orderStr[i2], guideStr);
		scanf_s("%d", &intFromUser[i2]);
	}

	printf("몫은 %d이고 나머지는 %d입니다.\n", intFromUser[0] / intFromUser[1], intFromUser[0] % intFromUser[1]);

	return 0;
}

// </First>