// <Second>

#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	char guideStr[] = "정수를 입력하시오: ";
	char * orderStr[] = { "백", "십", "일" };

	// Receiving integer from user
	int intFromUser;
	printf("%s", guideStr);
	scanf_s("%d", &intFromUser);

	{
		// Initializing 'acc1' to most significant decimal digit
		int acc1 = 1;
		for (int i2 = 1; i2 < ARRLEN(orderStr); i2++)
		{
			acc1 *= 10;
		}

		for (int i2 = 0; i2 < ARRLEN(orderStr); i2++, acc1 /= 10)
		{
			printf("%s의 자리수: %d\n", orderStr[i2], intFromUser / acc1 % 10);
			// expr:% 10 discards exceeding digits in front of the target digit
		}
	}

	return 0;
}

// </Second>