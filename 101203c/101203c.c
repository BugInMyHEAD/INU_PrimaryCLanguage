// <Third>

#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	char guideStr[] = "정수를 입력하시오: ";

	// Receiving integer from user
	int intFromUser;
	printf("%s", guideStr);
	scanf_s("%d", &intFromUser);

	// cubing 'intFromUser'
	int cubedIntFromUser = 1;
	for (int i2 = 0; i2 < 3; i2++)
	{
		cubedIntFromUser *= intFromUser;
	}

	printf("수식의 값은: %f\n", (double)( cubedIntFromUser - 20 ) / ( intFromUser - 7 ));

	return 0;
}

// </Third>