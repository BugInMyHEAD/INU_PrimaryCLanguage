// <Third>

#include <stdio.h>


#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif


#define HOW_MANY_NUMBERS_FROM_USER 2

int main(void)
{
	char guideStr[] = "개의 실수를 입력하시오: ";
	
	// Receiving real numbers from user
	double doubleFromUser[HOW_MANY_NUMBERS_FROM_USER];
	printf("%zd%s", ARRLEN(doubleFromUser), guideStr);
	for (int i2 = 0; i2 < ARRLEN(doubleFromUser); i2++)
	{
		scanf_s("%lf", &doubleFromUser[i2]);
	}

	{
		// sum of all of elements of doubleFromUser
		double acc1 = 0;
		for (int i2 = 0; i2 < ARRLEN(doubleFromUser); i2++)
		{
			acc1 += doubleFromUser[i2];
		}
		printf("합의 정수부 = %lld\n", (long long)acc1);
	}

	return 0;
}

// </Third>