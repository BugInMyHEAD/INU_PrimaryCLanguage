// <First>

#include <stdio.h>
#include <math.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

#define SQUARE(x) ( (x) * (x) )

typedef struct Point2D Point2D;
struct Point2D
{
	double x, y;
};

int main(void)
{
	char * orderStr[] = { "첫", "두" };
	Point2D pointFromUser[ARRLEN(orderStr)];

	// Receiving 2D points from user
	for (int i2 = 0; i2 < ARRLEN(orderStr); i2++)
	{
		printf("%s번째 점 (x%d, y%d): ", orderStr[i2], i2 + 1, i2 + 1);
		scanf_s("%lf %lf", &pointFromUser[i2].x, &pointFromUser[i2].y);
	}

	// Printing each point
	for (int i2 = 0; ; )
	{
		printf("x%d = %.0f, y%d = %.0f", i2 + 1, pointFromUser[i2].x, i2 + 1, pointFromUser[i2].y);
		if (++i2 < ARRLEN(orderStr))
		{
			printf(", ");
		}
		else
		{
			printf("\n");

			break;
		}
	}

	// Printing distance between 2 points
	printf("Distance= %f\n", 
		sqrt(
			SQUARE(pointFromUser[0].x - pointFromUser[1].x)
			+ SQUARE(pointFromUser[0].y - pointFromUser[1].y)
		)
	);

	return 0;
}

// </First>