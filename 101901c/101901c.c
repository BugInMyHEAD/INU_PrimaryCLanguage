// <LeapYear>

#include <stdio.h>

enum CommonLeap { COMMON, LEAP }; // for index of commonLeapStr
// common year 평년  leap year 윤년

int main(void)
{
	char * commonLeapStr[] = { "평", "윤" };

	// receiving year from user
	printf("연도를 입력하시오: ");
	int intFromUser;
	scanf_s("%d", &intFromUser);
	printf("%d년은 ", intFromUser);

	enum CommonLeap commonLeap; // for index of commonLeapStr
	if (intFromUser % 4)
	{
		commonLeap = COMMON; // when the year cannot be divided by 4
	}
	else if (intFromUser % 100)
	{
		commonLeap = LEAP;   // when the year can be divided by 4
		                     // but cannot be divided by 100
	}
	else if (intFromUser % 400)
	{
		commonLeap = COMMON; // when the year can be divided by 100
		                     // but cannot be divided by 400
	}
	else
	{
		commonLeap = LEAP;   // when the year can be divided by 400
	}
	printf("%s년입니다.\n", commonLeapStr[commonLeap]);

	return 0;
}

// </LeapYear>