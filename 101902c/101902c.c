// <Dice>

#include <stdio.h>
#include <stdlib.h>

enum IGa { I, GA }; // for index of iGaStr

int main(void)
{
	const char * iGaStr[] = { "이", "가" };

	srand((unsigned)time(NULL));

	printf("주사위를 던집니다.\n");
	int dice = rand() % 6 + 1; // 1~6 dice
	
	enum IGa iGa; // for index of iGaStr
	switch (dice % 10)
	{
	case 2:
	case 4:
	case 5:
	case 9:
		iGa = GA;
		break;
	default:
		iGa = I;
	}

	printf("%d%s 나왔습니다.\n", dice, iGaStr[iGa]);
	// ex. "1이 나왔습니다.", "2가 나왔습니다."

	return 0;
}

// </Dice>