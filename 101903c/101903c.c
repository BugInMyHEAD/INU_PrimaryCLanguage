// <Score>

#include <stdio.h>

int main(void)
{
	// receiving scores from user
	printf("중간고사 점수와 기말고사 점수를 입력하시오:\n");
	double doubleFromUser[2];
	scanf_s("%lf %lf", &doubleFromUser[0], &doubleFromUser[1]);
	double averageScore = ( doubleFromUser[0] + doubleFromUser[1] ) / 2;

	char gradeCh; // character variable for storing grade information
	if (averageScore >= 90.0)
	{
		gradeCh = 'A';
	}
	else if (averageScore >= 80.0)
	{
		gradeCh = 'B';
	}
	else if (averageScore >= 70.0)
	{
		gradeCh = 'C';
	}
	else if (averageScore >= 60.0)
	{
		gradeCh = 'D';
	}
	else // when 'averageScore' < 60.0
	{
		gradeCh = 'F';
	}
	printf("%c 학점\n", gradeCh);

	return 0;
}

// </Score>