// <Calculator>

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )

#include <stdio.h>

int main(void)
{
	// receiving expression from user
	printf("수식을 입력하시오: ");
	int intFromUser[2] = { -1, -1 };
	char chFromUser;
	scanf_s("%d %c %d", &intFromUser[0], &chFromUser, 1, &intFromUser[1]);
	
	// program exits when one of 'intFromUser's is negative
	for (int i2 = 0; i2 < ARRLEN(intFromUser); i2++)
	{
		if (intFromUser[i2] < 0)
		{
			printf("0 또는 양의 정수만 입력하세요.\n");

			return 0;
		}
	}

	int result;
	if ('+' == chFromUser)
	{
		result = intFromUser[0] + intFromUser[1];
	}
	else if ('-' == chFromUser)
	{
		result = intFromUser[0] - intFromUser[1];
	}
	else if ('*' == chFromUser)
	{
		result = intFromUser[0] * intFromUser[1];
	}
	else if ('/' == chFromUser)
	{
		if (0 == intFromUser[1]) // when the user tried to divide by 0
		{
			printf("0 으로 나누었습니다.\n");

			return 0;
		}
		else
		{
			result = intFromUser[0] / intFromUser[1];
		}
	}
	else // operation format exception
	{
		printf("지원하지 않는 연산자입니다.\n");

		return 0;
	}

	printf("%d\n", result);

	return 0;
}

// </Calculator>