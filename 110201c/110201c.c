// <assignment_1>

#include <stdio.h>

int main(void)
{
	printf("과제 1\n");

	// input from user
	printf("두 수를 입력: ");
	int intFromUser[2];
	scanf_s("%d %d", &intFromUser[0], &intFromUser[1]);

	int addResult = intFromUser[0] + intFromUser[1];

	int subtractResult;
	if (intFromUser[0] > intFromUser[1]) // when first input is greater than second one
	{
		subtractResult = intFromUser[0] - intFromUser[1];
	}
	else // when first input is less than or equal to second one
	{
		subtractResult = intFromUser[1] - intFromUser[0];
	}

	// printing
	printf("합 %d, 차 %d\n", addResult, subtractResult);

	return 0;
}

// </assignment_1>