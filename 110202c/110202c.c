// <assignment_2>

#include <stdio.h>
#include <ctype.h>

int main(void)
{
	printf("과제 2\n");

	// input from user
	printf("신호등의 색깔 입력: ");
	int charFromUser = getchar();

	char * trafficSignStr = "";
	switch (toupper(charFromUser))
	{
	case 'G':
		trafficSignStr = "진행";
		break;
	case 'R':
		trafficSignStr = "정지";
		break;
	case 'Y':
		trafficSignStr = "주의";
		break;
	}

	// printing
	printf("%s\n", trafficSignStr);

	return 0;
}

// </assignment_2>