// <assignment_4>

#include <stdio.h>
#include <ctype.h>
#include <math.h>

// 미국 달러, 유럽연합 유로, 일본 엔, 중국 위안, 영국 파운드, 태국 바트, 홍콩 달러
enum Currency { USD, EUR, JPY, CNY, GBP, THB, HKD };
const double EXCHANGERATE2KRW[] =
{
	1152.42, 1358.9, 10.2087, 179.43, 1519.86, 35.86, 147.8
};
const char* currencyKorName[] = { "달러", "유로", "엔", "위안", "파운드", "바트", "홍콩달러" };

int main(void)
{
	printf("과제 4\n");

	// input from user: <KRW> <nation character>
	printf("환전할 원화와 국가 코드를 입력하세요: ");
	double krwAmount;
	char nationCh;
	scanf_s("%lf", &krwAmount);
	while (!isalpha(nationCh = getchar())); // only alphabet char in 'nationCh'

	// exchanging
	enum Currency currency;
	switch (toupper(nationCh))
	{
	case 'U':
		currency = USD;
		break;
	case 'E':
		currency = EUR;
		break;
	case 'Y':
		currency = JPY;
		break;
	case 'W':
		currency = CNY;
		break;
	case 'P':
		currency = GBP;
		break;
	case 'B':
		currency = THB;
		break;
	case 'H':
		currency = HKD;
		break;
	default:
		printf("지원하지 않는 국가\n");

		return 0;
	}
	double exchangedAmount = krwAmount / EXCHANGERATE2KRW[currency];

	// rounding
	double modVal = JPY == currency ? 10 : 1; // 엔은 10엔 단위로, 나머지는 1 통화단위로
	double fracOfExchangedAmount = fmod(exchangedAmount, modVal);
	double roundedExchangedAmount = exchangedAmount - fracOfExchangedAmount;

	// getting remainder
	double remainder = fracOfExchangedAmount * EXCHANGERATE2KRW[currency];
	double roundedRemainder = ceil(remainder);
	
	// printing
	printf("%.0f %s, %.0f %s\n", roundedExchangedAmount, currencyKorName[currency], remainder, "원");

	return 0;
}

// </assignment_4>