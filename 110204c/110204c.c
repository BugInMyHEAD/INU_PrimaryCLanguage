// <assignment_5>

#include <stdio.h>
#include <math.h>

int main(void)
{
	printf("과제 5\n");

	// input from user
	// a = doubleFromUser[2], b = doubleFromUser[1], c = doubleFromUser[0]
	printf("ax2 + bx + c = 0 에서 a, b, c를 차례로 입력하세요: ");
	double doubleFromUser[3];
	scanf_s("%lf %lf %lf", &doubleFromUser[2], &doubleFromUser[1], &doubleFromUser[0]);

	int imaginaryFlag = 0;
	double sol[2];
	if (0 == doubleFromUser[2])
	{
		sol[0] = sol[1] = -doubleFromUser[0] / doubleFromUser[1]; // -c/b
	}
	else
	{
		double discriminant = doubleFromUser[1] * doubleFromUser[1] - 4 * doubleFromUser[2] * doubleFromUser[0];
		if (discriminant < 0)
		{
			imaginaryFlag = 1; // flag is set if b2-4ac < 0
		}
		else
		{
			double dA3 = -doubleFromUser[1] / (2 * doubleFromUser[2]); // -b/2a
			double dB3 = sqrt(discriminant) / (2 * doubleFromUser[2]); // sqrt(b2-4ac)/2a
			sol[0] = dA3 - dB3;
			sol[1] = dA3 + dB3;
		}
	}

	// printing
	if (imaginaryFlag)
	{
		printf("허근");
	}
	else
	{
		printf("%f", sol[0]);
		if (sol[0] != sol[1])
			printf(", %f", sol[1]);
	}
	puts("");

	return 0;
}

// </assignment_5>