#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	int interval[] = { 1, 2, 5 }; // 인덱스는 각 실습 문제 #1, #2, #3
	                              // 값들은 각 실습 문제에서의 수열 간격 및 시작점
	{
		int i1 = 0; // 각 실습 문제 인덱스 (#1, #2, #3)
		while (i1 < ARRLEN(interval))
		{
			printf("%d의 배수\n", interval[i1]);

			int i3 = interval[i1]; // 각 실습 문제에서의 시작점 (1, 2, 5) 및 출력되는 수
			while (i3 <= 100)
			{
				// 10개의 수 출력 후 CR
				int i5 = 0;
				while (i5 < 10)
				{
					printf("%-8d", i3);

					i3 += interval[i1];
					i5++;
				}
				puts("");
			}
			puts("");

			i1++;
		}
	}
}