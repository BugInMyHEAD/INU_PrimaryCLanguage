#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
	srand((unsigned)time(NULL));

	int min = 1;
	int range = 100;

	int answer = rand() % range + min; // 정답
	printf("(정답은 %d)\n", answer);

	int guess = min - 1;
	int tries = 0;
	while (guess != answer)
	{
		printf("정답을 추측하여 보시오: ");
		scanf_s("%d", &guess);
		tries++;
		if (guess >answer) // 사용자가 입력한 정수가 정답보다 높으면
			printf("제시한 정수가 높습니다.\n");
		if (guess <answer) // 사용자가 입력한 정수가 정답보다 낮으면
			printf("제시한 정수가 낮습니다.\n");
	}

	printf("축하합니다. 시도횟수=%d\n", tries);

	return 0;
}
