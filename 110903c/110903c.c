#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef enum RSP RSP;
enum RSP { ROCK, SCISSORS, PAPER };
const size_t RSPSIZE = 3;

typedef enum LDW LDW;
enum LDW { LOSE = -1, DRAW, WIN };

RSP counter(RSP opponent) // opponent를 이기는 가위바위보 값 반환
{
	switch (opponent)
	{
	case ROCK:
		return PAPER;
	case SCISSORS:
		return ROCK;
	case PAPER:
		return SCISSORS;
	default:
		exit(1);
	}
}

LDW result(RSP self, RSP opponent) // self를 기준으로 opponent와의 가위바위보 결과 반환
{
	if (self == opponent)
		return DRAW;
	else if (self == counter(opponent)) // self가 opponent를 이기는 가위바위보 값일 때
		return WIN;
	else
		return LOSE;
}

int main(void)
{
	srand((unsigned)time(NULL));

	char * rspKorean[] = { "바위", "가위", "보" }; // ROCK, SCISSORS, PAPER
	char * resultStr[] = { "졌습니다", "비겼습니다", "이겼습니다" }; // LOSE, DRAW, WIN

	printf("컴퓨터와 가위바위보를 합시다!\n\n");
	while (1)
	{
		// 사용자로부터 가위바위보 값 입력받음
		printf("가위:1 바위:2 보:3 >> ");
		int intFromStdin = INT_MIN;
		scanf_s("%d", &intFromStdin);
		RSP player = -1;
		switch (intFromStdin)
		{
		case 1:
			player = SCISSORS;
			break;
		case 2:
			player = ROCK;
			break;
		case 3:
			player = PAPER;
			break;
		default:
			goto __rsp_game_exit__;
		}

		RSP computer = rand() % RSPSIZE + ROCK; // 컴퓨터의 가위바위보 값 정함

		// 각자의 가위바위보 값 출력
		printf("나:%s 컴퓨터:%s\n", rspKorean[player], rspKorean[computer]);
		// 게임 결과 출력
		printf("%s\n\n", resultStr[result(player, computer) - LOSE]); // LOSE = -1
	}

__rsp_game_exit__:;
	return 0;
}
