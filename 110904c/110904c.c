#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

#define NO_OF_DICE 2

int main(void)
{
	srand((unsigned)time(NULL));

	char * playerStr[] = { "컴퓨터1", "컴퓨터2" };
	int dice[ARRLEN(playerStr)][NO_OF_DICE]; // 1차원 인덱스는 각자의 각각의 주사위의 값
	                                         // 2차원 인덱스는 각 참여자
	int diceSum[ARRLEN(playerStr)];

	int diceMin = 1;
	int diceRange = 6;

	while (1)
	{
		{
			int i3 = 0;
			while (i3 < ARRLEN(playerStr))
			{
				printf("%s의 주사위: ", playerStr[i3]);
				diceSum[i3] = 0;
				int i5 = 0;
				while (i5 < NO_OF_DICE)
				{
					// 주사위 값 난수로 구하고 합 구하기
					diceSum[i3] += dice[i3][i5] = rand() % diceRange + diceMin;
					printf("%d ", dice[i3][i5]); // 각자의 주사위 값 출력

					i5++;
				}
				printf("합: %d\n", diceSum[i3]);

				i3++;
			}
		}
		puts("");

		// 주사위 합 중 최대값 구하기
		int diceSumMax = INT_MIN;
		{
			int i3 = 0;
			while (i3 < ARRLEN(playerStr))
			{
				if (diceSumMax < diceSum[i3]) diceSumMax = diceSum[i3];

				i3++;
			}
		}

		// 주사위 합 중 최대값을 가진 참여자 목록 출력
		{
			int i3 = 0;
			while (i3 < ARRLEN(playerStr))
			{
				if (diceSumMax <= diceSum[i3])
					printf("%s 이겼습니다\n", playerStr[i3]);

				i3++;
			}
		}

		// 사용자에게 다음 게임을 진행할지 물어봄
		printf("계속하시겠습니까? (y/n)>> ");
		char chFromStdin;
		while (!isalpha(chFromStdin = getchar())); // chFromStdin에 알파벳만 들어감
		puts("");
		switch (toupper(chFromStdin))
		{
		case 'Y':
			continue;
		case 'N':
			break;
		}

		break;
	}

	return 0;
}
