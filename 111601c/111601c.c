#include <stdio.h>

int main(void)
{
	for (int i2 = 2; i2 <= 9; i2++)
	{
		for (int i4 = 1; i4 <= 9; i4++)
		{
			printf("%d X %d = %-2d   ", i2, i4, i2 * i4);
		}
		puts("");
	}

	return 0;
}