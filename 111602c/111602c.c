#include <stdio.h>

int main(void)
{
	for (int i2 = 1; i2 <= 9; i2++)
	{
		for (int i4 = 2; i4 <= 9; i4++)
		{
			printf("%d X %d = %-2d   ", i4, i2, i2 * i4);
		}
		puts("");
	}

	return 0;
}