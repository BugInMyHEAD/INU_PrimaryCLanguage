#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	for (int i2 = 1; i2 <= 9; i2++)
	{
		for (int i4 = 2; i4 <= 9; i4++)
		{
			printf("%d X %d = ", i4, i2);

			int mulVal = i2 * i4;

			if (9 <= i4)
			{
				printf("%d", mulVal);

				break;
			}

			char buf5[5];
			sprintf_s(buf5, ARRLEN(buf5), "%d,", mulVal);
			printf("%-5s", buf5);
		}
		puts("");
	}

	return 0;
}