#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	int interval[] = { 1, -1 };
	int line[] = { 5, line[0] - 1 };
	int initialNoOfStamp = 0;

	for (int phase = 0, noOfStamp = initialNoOfStamp; phase < ARRLEN(interval); phase++)
	{
		for (int lineIdx = 0; lineIdx < line[phase]; lineIdx++)
		{
			noOfStamp += interval[phase];

			for (int stampCount = 0; stampCount < noOfStamp; stampCount++)
			{
				printf("*");
			}
			puts("");
		}
	}

	return 0;
}