#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	int scores[5];
	for (int i2 = 0; i2 < ARRLEN(scores); i2++)
	{
		scores[i2] = 10 * ( i2 + 1 );
	}

	for (size_t i2 = 0; i2 < ARRLEN(scores); i2++)
	{
		printf("scores[%zd]=%d\n", i2, scores[i2]);
	}

	return 0;
}