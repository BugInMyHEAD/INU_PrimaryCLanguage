#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	int a[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	int b[10] = { 0, 2, 4, 6, 8, 10, 12, 14, 16, 18 };

	for (size_t i2 = 0; i2 < ARRLEN(a); i2++)
	{
		int sum = a[i2] + b[i2];
		double avg = (double)sum / 2;
		printf("[%zd] a=%d, b=%d, sum=%d, avg=%.2f\n", i2, a[i2], b[i2], sum, avg);
	}

	return 0;
}