#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	int a[3][3];
	for (size_t i2 = 0; i2 < ARRLEN(a); i2++)
	{
		for (size_t i4 = 0; i4 < ARRLEN(*a); i4++)
		{
			a[i2][i4] = 0;
		}
	}

	for (size_t i2 = 0; i2 < ARRLEN(a); i2++)
	{
		for (size_t i4 = 0; i4 < ARRLEN(*a); i4++)
		{
			printf("%d ", a[i2][i4]);
		}
		puts("");
	}

	return 0;
}