#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	srand((unsigned)time(NULL));

	int array[100];
	for (size_t i2 = 0; i2 < ARRLEN(array); i2++)
	{
		array[i2] = rand() % 100;
	}

	size_t column = 10; // 각 행마다 10개의 수
	size_t line = ( ARRLEN(array) + column - 1 ) / column; // ceiling 하여 필요한 라인 수 구함
	for (size_t i2 = 0; i2 < line; i2++)
	{
		for (size_t i4 = 0; i4 < column; i4++)
		{
			printf("%4d", array[i2 * 10 + i4]);
		}
		puts("");
	}

	size_t maxIdx = 0, minIdx = 0; // 배열 크기 1 이상이어야 논리적 오류 배제 가능: 단점
	for (size_t i2 = 1; i2 < ARRLEN(array); i2++)
	{
		if (array[maxIdx] < array[i2]) maxIdx = i2;
		if (array[i2] < array[minIdx]) minIdx = i2;
	}

	// 처음에 나오는 최대값, 최소값의 인덱스만 표시
	printf("최대값 %d, 인덱스 %zd\n최소값 %d, 인덱스 %zd\n", array[maxIdx], maxIdx, array[minIdx], minIdx);

	return 0;
}