#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

int main(void)
{
	srand((unsigned)time(NULL));

	// making formatStr = "%5d"
	int digitLength = 5;
	char precursorStr[] = "%%%dd";
	char formatStr[BUFSIZ];
	sprintf_s(formatStr, ARRLEN(formatStr), precursorStr, digitLength);

	int matrix[2][3][3];
	for (size_t i2 = 0; i2 < ARRLEN(matrix); i2++) // �� ���
	{
		for (size_t i4 = 0; i4 < ARRLEN(*matrix); i4++) // ��
		{
			for (size_t i6 = 0; i6 < ARRLEN(**matrix); i6++) //��
			{
				matrix[i2][i4][i6] = rand() % 99;
			}
		}
	}

	for (size_t i2 = 0; i2 < ARRLEN(*matrix); i2++) // ������ ����� �� ������ CR
	{
		for (size_t i4 = 0; i4 < ARRLEN(matrix); i4++) // �� ��� ���̿� ���� ����
		{
			for (size_t i6 = 0; i6 < ARRLEN(**matrix); i6++) // �� ����� ���� ���� ���
			{
				printf(formatStr, matrix[i4][i2][i6]); // ���� ���
			}
			printf("        "); // ���� ����
		}
		puts(""); // CR
	}

	// ����� ����, ���� ����� ����
	int addedMatrix[ARRLEN(*matrix)][ARRLEN(**matrix)];
	int subtractedMatrix[ARRLEN(*matrix)][ARRLEN(**matrix)];
	for (size_t i2 = 0; i2 < ARRLEN(addedMatrix); i2++)
	{
		for (size_t i4 = 0; i4 < ARRLEN(addedMatrix); i4++)
		{
			addedMatrix[i2][i4] = matrix[0][i2][i4] + matrix[1][i2][i4];
			subtractedMatrix[i2][i4] = matrix[0][i2][i4] - matrix[1][i2][i4];
		}
	}

	// ����� ���� ����� ���
	printf("\n����\n");
	for (size_t i2 = 0; i2 < ARRLEN(addedMatrix); i2++)
	{
		for (size_t i4 = 0; i4 < ARRLEN(addedMatrix); i4++)
		{
			printf(formatStr, addedMatrix[i2][i4]);
		}
		puts("");
	}

	//����� ���� ����� ���
	printf("\n����\n");
	for (size_t i2 = 0; i2 < ARRLEN(addedMatrix); i2++)
	{
		for (size_t i4 = 0; i4 < ARRLEN(addedMatrix); i4++)
		{
			printf(formatStr, subtractedMatrix[i2][i4]);
		}
		puts("");
	}

	return 0;
}