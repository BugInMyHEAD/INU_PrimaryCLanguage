#include <stdio.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

double FtoC(double temp_f)
{
	double temp_c;
	temp_c = (temp_f - 32) * 5 / 9;

	return temp_c;
}

double CtoF(double temp_c)
{
	double temp_f;
	temp_f = temp_c * 9 / 5 + 32;

	return temp_f;
}

int main(void)
{
	for (;;)
	{
		double c, f;

		printf("1:Farenheit to Celsius\n2:Celsius to Farenheit\nother:exit\n>");

		int funcSelectCode;
		scanf_s("%d", &funcSelectCode);
		switch (funcSelectCode)
		{
		case 1:
			printf("ȭ���µ�:");
			scanf_s("%lf", &f);
			c = FtoC(f);
			printf("ȭ���µ� %f�� �����µ� %f�� �ش��Ѵ�.\n", f, c);
			break;

		case 2:
			printf("�����µ�:");
			scanf_s("%lf", &c);
			f = CtoF(c);
			printf("�����µ� %f�� ȭ���µ� %f�� �ش��Ѵ�.\n", c, f);
			break;

		default:
			return 0;
		}
	}
}