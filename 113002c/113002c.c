#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <ctype.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

void pick(int* base, int num, int min, int max)
{
	if (max - min < num)
		exit(1);
	if (min < 0 || max < 0)
		exit(2);

	for (size_t i2 = 0; i2 < num; )
	{
		base[i2] = rand() % (max - min) + min;
		int j2;
		for (j2 = 0; j2 < i2; j2++)
		{
			if (base[i2] == base[j2])
			{
				break;
			}
		}
		if (j2 >= i2)
		{
			i2++;
		}
	}
}

void printLotteryNumbers()
{
	int arr[7];

	pick(arr, ARRLEN(arr), 1, 45 + 1);

	for (size_t i2 = 0; i2 < ARRLEN(arr); i2++)
	{
		printf("%4d", arr[i2]);
	}
	puts("");
}

void job()
{
	for (;;)
	{
		printLotteryNumbers();

		printf("< c:continue / x:exit >");
		for (;;)
		{
			switch (tolower(_getch()))
			{
			case 'c':
				puts("");
				break;
			case 'x':
				puts("");
				goto __job_exit;
			default:
				continue;
			}

			break;
		}
	}
__job_exit:;
}

int main(void)
{
	srand((unsigned)time(NULL));

	job();

	return 0;
}