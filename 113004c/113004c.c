#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <ctype.h>

#ifndef __CUSTOMDEF_H__
#define __CUSTOMDEF_H__

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )
#define BITSIZEOF(x) ( sizeof(x) << 3 )
#define BYTE2BITS(x) ( (x) << 3 )

#endif

void job()
{
	double arr[10] = { 1., 2., 3., 4., 5., 6., 7., 8., 9., 10. };
	double * p = arr;
	double * arrLast = &arr[ARRLEN(arr) - 1];
	for (; p <= arrLast; p++)
	{
		printf("value:%5.2f, address:%p\n", *p, p);
	}
	puts("");
	for (size_t i2 = 0; i2 < ARRLEN(arr); i2++)
	{
		printf("value:%5.2f, address:%p\n", arr[i2], arr + i2);
	}
	puts("");
}

int main(void)
{
	job();

	return 0;
}